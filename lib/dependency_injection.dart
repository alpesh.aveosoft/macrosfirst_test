enum Flavor { MOCK, PROD }

//DI
class Injector {
  static Flavor flavor;

  static void configure(Flavor fla) {
    flavor = fla;
  }
}
