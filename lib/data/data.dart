import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/dependency_injection.dart';
import 'package:flutter_app/model/comment.dart';
import 'package:flutter_app/model/post.dart';
import 'package:http/http.dart' as http;

class PostData {
  String postsUrl = 'https://jsonplaceholder.typicode.com/posts';
  String commentsUrl = 'https://jsonplaceholder.typicode.com/comments';
  int POST = 0;
  int COMMENT = 1;

  Future<List<dynamic>> getPosts(BuildContext context) async {
    final data = await getData(context, postsUrl, POST);
    var data1 = data.map((c) => new Post.fromMap(c)).toList();
    return data1;
  }

  getJson(int type) {
    if (type == POST)
      return "assets/posts.json";
    else
      return "assets/comments.json";
  }

  Future<dynamic> getData(BuildContext context, String url, int type) async {
    var jsonResult;
    if (isProduction()) {
      http.Response response = await http.get(url);
      jsonResult = json.decode(response.body);
    } else {
      String data =
          await DefaultAssetBundle.of(context).loadString(getJson(type));
      jsonResult = json.decode(data);
    }
    return jsonResult;
  }

  bool isProduction() {
    if (Injector.flavor == Flavor.PROD)
      return true;
    else
      return false;
  }

  Future<List<dynamic>> getComments(BuildContext context, int id) async {
    final data = await getData(context, commentsUrl, COMMENT);
    List<dynamic> comments = data.map((c) => new Comment.fromMap(c)).toList();
    List<dynamic> filteredComments = List();
    // List<dynamic> filteredComments = comments.where((comment) => comment['postId'] == id);
    for (var i = 0; i < comments.length; i++) {
      Comment comment = comments[i];
      if (id == comment.postId) {
        filteredComments.add(comment);
      }
    }
    return filteredComments;
  }
}
