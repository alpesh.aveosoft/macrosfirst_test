import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/data.dart';
import 'package:flutter_app/views/post_tile.dart';
import 'package:material_segmented_control/material_segmented_control.dart';

import '../dependency_injection.dart';
import 'comment_view.dart';

class Post_view extends StatefulWidget {
  static String id = "Post_View";

  Post_view({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _Post_viewState createState() => _Post_viewState();
}

class _Post_viewState extends State<Post_view> {
  PostData postData;
  int selectionIndex = 0;
  @override
  void initState() {
    super.initState();
    postData = PostData();
  }

  Map<int, Widget> _children = {
    0: SegmentItem(text: 'MOCK'),
    1: SegmentItem(text: 'PROD'),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: [
          MaterialSegmentedControl(
              children: _children,
              selectionIndex: selectionIndex,
              onSegmentChosen: (index) {
                setState(() {
                  if (index == 0)
                    Injector.configure(Flavor.MOCK);
                  else
                    Injector.configure(Flavor.PROD);
                  selectionIndex = index;
                  postData.getPosts(context);
                });
              },
              unselectedColor: Colors.white,
              selectedColor: Colors.deepOrange)
        ],
      ),
      body: FutureBuilder<List<dynamic>>(
        future: postData.getPosts(context),
        builder: (context, posts) {
          if (posts.data != null) {
            return ListView.builder(
              itemCount: posts.data.length,
              itemBuilder: (BuildContext context, int index) => PostTile(
                context: context,
                post: posts.data[index],
                onClick: () {
                  Navigator.push(
                    context,
                    PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) =>
                          Comment_view(
                        title: 'Comments',
                        post: posts.data[index],
                      ),
                    ),
                  );
                },
              ),
            );
          } else {
            return Container();
          }
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class SegmentItem extends StatelessWidget {
  SegmentItem({this.text, this.fontSize});

  final String text;
  double fontSize = 13;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 5,
      child: Center(
        child: Text(
          text,
          style: TextStyle(fontSize: fontSize),
        ),
      ),
    );
  }
}
