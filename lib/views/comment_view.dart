import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/data.dart';
import 'package:flutter_app/model/comment.dart';
import 'package:flutter_app/model/post.dart';
import 'package:flutter_app/views/post_tile.dart';

class Comment_view extends StatefulWidget {
  static String id = "Comment_View";
  Post post;
  Comment_view({Key key, this.title, this.post}) : super(key: key);

  final String title;

  @override
  _Comment_viewState createState() => _Comment_viewState();
}

class _Comment_viewState extends State<Comment_view> {
  PostData postData;
  @override
  void initState() {
    super.initState();
    postData = PostData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              height: 160,
              child: PostTile(context: context, post: widget.post)),
          Expanded(
            flex: 8,
            child: Container(
              margin: const EdgeInsets.all(20.0),
              child: FutureBuilder<List<dynamic>>(
                future: postData.getComments(context, widget.post.id),
                builder: (context, comments) {
                  if (comments.data != null) {
                    if (comments.data.length != 0) {
                      return ListView.builder(
                        itemCount: comments.data.length,
                        itemBuilder: (BuildContext context, int index) =>
                            _getRow(comments.data[index]),
                      );
                    } else {
                      return Center(
                        child: Text('No comments'),
                      );
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _getRow(Comment comment) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      elevation: 5,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.grey.shade300,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        comment.name,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        comment.email,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
              child: Text(comment.body),
            ),
            // Text(post.body)
          ],
        ),
      ),
    );
  }
}
