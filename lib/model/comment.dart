class Comment {
  int postId;
  String name;
  String email;
  String body;
  Comment({this.name, this.email, this.body});

  Comment.fromMap(Map<String, dynamic> map)
      : postId = map['postId'],
        name = map['name'].toString(),
        email = map['email'].toString(),
        body = map['body'].toString();
}
