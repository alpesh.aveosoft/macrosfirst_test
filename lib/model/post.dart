class Post {
  int id;
  String title;
  String subTitle;
  String body;
  Post({this.title, this.subTitle, this.body});

  Post.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        title = map['title'].toString(),
        subTitle = map['title'].toString(),
        body = map['body'].toString();
}
