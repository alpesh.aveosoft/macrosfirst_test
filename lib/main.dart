import 'package:flutter/material.dart';
import 'package:flutter_app/views/comment_view.dart';
import 'package:flutter_app/views/post_view.dart';

import 'dependency_injection.dart';

void main() {
  //Change Flavor to switch between MOCK and PRODUCTION data
  Injector.configure(Flavor.MOCK);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MacrosFirst Test app',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: Post_view.id,
      routes: {
        Post_view.id: (context) => Post_view(
              title: 'Posts',
            ),
        Comment_view.id: (context) => Comment_view(
              title: 'Comments',
            ),
      },
    );
  }
}
